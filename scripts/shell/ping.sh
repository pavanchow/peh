#!/bin/bash
# defining shell script

echo "*** RECOMMENDED TO SEND THE OUTPUT TO A FILE ***"
echo "*** Like ./ping.sh >> file.txt ***"
read -p "Enter first 2 octets of IP address(192.168) : " ip
#Asks for first two octets of IP range that user want to scan and assigns it to variable ip


if [ "$ip" == "" ]
# if user doen't input any IP address, the following code will be executed else it will go to for loop
then 
	echo "***Please type an IP address***"
	echo "***Like 192.168 ***"
	# If user inputs blank spaces then the above line explain what to input
else
	# the following is a nested for loop for scanning last 2 octets of an IP address
	for octet1 in `seq 1 254`; do
		# octet1 will be third octet of the IP address range which we are going to scan 
		for octet2 in `seq 1 254`; do
		# octet2 will the last(fourth) octet of the IP range
				ping -c 1 $ip.$octet1.$octet2 | grep "64 bytes" | cut -d " " -f 4 | tr -d ":" &
				# Ping will send ICMP packet(ping) of count 1 (i.e., one time) to the IP address
				# grep will pull all the successful connections 
				# cut will put 4 the word of current line (i.e., IP address)
				# tr will remove colon(:) from the end of the IP address
				done
				#closing octet2
		done
		#closing octet1
fi
#closing the program